package trenitalia.com.digitalmaintenance.utils;

/**
 * Created by davidericci on 25/08/17.
 */

public class IntentsProvider {
    //SELECT PROFILE
    public static final String ROLE_ITEM_TO_ROTABILI = "role_item_to_rotabili";
    public static final String IMPLANT_ITEM_TO_ROTABILI = "implant_item_to_rotabili";
    public static final String ROTABILE_ITEM_TO_AVVISI = "rotabile_item_to_avvisi";
    public static final String AVVISO_ITEM_TO_DETAILAVVISO = "avviso_item_to_detailavviso";
    public static final String IS_MONITORAGGIO_TO_DETAILAVVISO = "is_monitoraggio_to_detailavviso";

    public enum AVVISI_TYPE {
        ZA, ZB, ZC, ZD, ZS, ZP
    }

    public enum TAB_TYPE {
        DETAIL, INTERVENTION, TREE, ATTACHMENT
    }
}
