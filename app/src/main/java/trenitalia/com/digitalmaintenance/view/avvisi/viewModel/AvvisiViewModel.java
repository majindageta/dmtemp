package trenitalia.com.digitalmaintenance.view.avvisi.viewModel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.databinding.ActivityAvvisiBinding;
import trenitalia.com.digitalmaintenance.databinding.ActivityMonitoraggioAvvisiBinding;
import trenitalia.com.digitalmaintenance.datamodel.Attachment;
import trenitalia.com.digitalmaintenance.datamodel.Avviso;
import trenitalia.com.digitalmaintenance.datamodel.Intervento;
import trenitalia.com.digitalmaintenance.datamodel.Rotabile;
import trenitalia.com.digitalmaintenance.datamodel.TreeComponent;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.view.avvisi.AvvisiDetailActivity;
import trenitalia.com.digitalmaintenance.view.avvisi.adapter.AvvisiAdapter;
import trenitalia.com.digitalmaintenance.view.selectProfile.SelectProfileActivity;

/**
 * Created by davidericci on 25/08/17.
 */

public class AvvisiViewModel {

    private Context context;
    private AvvisiAdapter adapter;
    private ArrayList<Avviso> availableAvvisi = new ArrayList<>();
    private List<String> listDataHeader; //ordini di lavoro + nessun ordine
    private HashMap<String, List<Avviso>> listDataChild; //key = ordine di lavoro value -> list di avvisi
    private int counterAvvisi = 0;
    private Rotabile rotabile;
    private Boolean isMonitoraggio = false;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public AvvisiViewModel(Context context, ActivityAvvisiBinding binding, Rotabile rotabile) {
        super();

        getAvvisi();
        this.context = context;
        this.rotabile = rotabile;

        populateData();
        binding.setVariable(BR.avvisiToHandleNumber, "" + counterAvvisi);
        this.adapter = new AvvisiAdapter(context, availableAvvisi, listDataHeader, listDataChild, false);
    }

    public AvvisiViewModel(Context context, ActivityMonitoraggioAvvisiBinding binding) {
        super();

        getAvvisiForMonitor();
        this.context = context;
        isMonitoraggio = true;
        populateDataForMonitoraggio();
        this.adapter = new AvvisiAdapter(context, availableAvvisi, listDataHeader, listDataChild, true);
    }

    private void populateData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Avviso>>();
        List<Avviso> temporaryList = new ArrayList<>();
        for (Avviso avviso : availableAvvisi) {
            if (!listDataHeader.contains(avviso.getWorkOrderNumber())) {
                listDataHeader.add(avviso.getWorkOrderNumber());
                listDataChild.put(avviso.getWorkOrderNumber(), new ArrayList<Avviso>());
            }
            temporaryList = listDataChild.get(avviso.getWorkOrderNumber());
            temporaryList.add(avviso);
            listDataChild.put(avviso.getWorkOrderNumber(), temporaryList);
            if (avviso.getWorkOrderNumber() != null && avviso.getWorkOrderNumber().length() > 0) {
                counterAvvisi++;
            }
        }
    }

    private void populateDataForMonitoraggio() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Avviso>>();
        List<Avviso> temporaryList = new ArrayList<>();
        for (Avviso avviso : availableAvvisi) {
            if (!listDataHeader.contains(avviso.getCreationDate())) {
                listDataHeader.add(avviso.getCreationDate());
                listDataChild.put(avviso.getCreationDate(), new ArrayList<Avviso>());
            }
            temporaryList = listDataChild.get(avviso.getCreationDate());
            temporaryList.add(avviso);
            listDataChild.put(avviso.getCreationDate(), temporaryList);
            if (avviso.getCreationDate() != null && avviso.getCreationDate().length() > 0) {
                counterAvvisi++;
            }
        }
    }

    public void initializeListView(ExpandableListView listView, Boolean isMonitoraggio) {

        sortList(isMonitoraggio);
        listView.setAdapter(adapter);

        for (int i = 0; i < listDataHeader.size(); i++) {
            listView.expandGroup(i); //expand everything
        }
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                expandableListView.expandGroup(i);
                return true;//allgroups remain open
            }
        });
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                String key = listDataHeader.get(i);
                Avviso value = listDataChild.get(key).get(i1);
                onAvvisoClick(value);
                return true;
            }
        });
        listView.setGroupIndicator(null); //no indicator for group
        listView.setChildIndicator(null);
        listView.setDividerHeight(0); //divider for child
    }

    private void sortList(Boolean inverted) {
        if (inverted)
            Collections.sort(listDataHeader, Collections.<String>reverseOrder());
        else
            Collections.sort(listDataHeader);

        HashMap<String, List<Avviso>> supportMap = new HashMap<>();
        SortedSet<String> keys;
        if (inverted) {
            keys = new TreeSet<>(Collections.<String>reverseOrder());
            keys.addAll(listDataChild.keySet());
        } else {
            keys = new TreeSet<>(listDataChild.keySet());
        }
        for (String key : keys) {
            supportMap.put(key, listDataChild.get(key));
        }
        listDataChild.clear();
        listDataChild.putAll(supportMap);
    }

    private void getAvvisi() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        //FAKE DATA
        Avviso avviso0 = new Avviso();
        avviso0.setType("ZD");
        avviso0.setCarriage("654654654");
        try {
            avviso0.setDate(sdf.parse("21/07/2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        avviso0.setCreationImplant("MANTOVA");
        avviso0.setFaultText("FAULT");
        avviso0.setGroup("GROUP");
        avviso0.setFreeText("***");
        avviso0.setInterventionNumber("123123");
        avviso0.setInterventionText("prova");
        avviso0.setLimitation("250 Km/h");
        avviso0.setLongText("long text");
        avviso0.setNumber("999");
        avviso0.setUserState("user state");
        avviso0.setSystemState("system state");
        avviso0.setShortText("short text");
        avviso0.setRotabileSeries("rotabile series");
        avviso0.setWorkOrderNumber("");
        avviso0.setStatus(avviso0.getWorkOrderNumber());

        Intervento i0 = new Intervento();
        i0.setShortText("short text");
        i0.setCode("code");
        i0.setGroup("group");
        i0.setInterventionText("inter aaa");
        i0.setPosition("position");

        TreeComponent t0 = new TreeComponent();
        t0.setCode("123");
        t0.setDescription("description");

        Attachment a0 = new Attachment();
        a0.setCode("1");
        a0.setDescription(" dest.writeList(treeComponents); dest.writeList(treeComponents); ");

        ArrayList<Intervento> interventoArrayList0 = new ArrayList<>();
        interventoArrayList0.add(i0);

        ArrayList<TreeComponent> treeComponentArrayList0 = new ArrayList<>();
        treeComponentArrayList0.add(t0);

        ArrayList<Attachment> attachmentArrayList = new ArrayList<>();
        attachmentArrayList.add(a0);

        avviso0.setInteventionList(interventoArrayList0);
        avviso0.setTreeComponents(treeComponentArrayList0);
        avviso0.setAttachments(attachmentArrayList);

        Avviso avviso1 = new Avviso();
        avviso1.setType("ZD");
        avviso1.setCarriage("654654654");
        try {
            avviso0.setDate(sdf.parse("21/07/2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        avviso1.setCreationImplant("MANTOVA");
        avviso1.setFaultText("FAULT");
        avviso1.setGroup("GROUP");
        avviso1.setFreeText("***");
        avviso1.setInterventionNumber("123123");
        avviso1.setInterventionText("prova");
        avviso1.setLimitation("250 Km/h");
        avviso1.setLongText("long text");
        avviso1.setNumber("999");
        avviso1.setUserState("user state");
        avviso1.setSystemState("system state");
        avviso1.setShortText("short text");
        avviso1.setRotabileSeries("rotabile series");
        avviso1.setWorkOrderNumber("1");
        avviso1.setStatus(avviso1.getWorkOrderNumber());

        Intervento i1 = new Intervento();
        i1.setShortText("short text");
        i1.setCode("code");
        i1.setGroup("group");
        i1.setInterventionText("inter aaa");
        i1.setPosition("position");

        TreeComponent t1 = new TreeComponent();
        t1.setCode("123");
        t1.setDescription("description");

        Attachment a1 = new Attachment();
        a1.setCode("1");
        a1.setDescription(" dest.writeList(treeComponents); dest.writeList(treeComponents); ");

        ArrayList<Intervento> interventoArrayList = new ArrayList<>();
        interventoArrayList.add(i1);

        ArrayList<TreeComponent> treeComponentArrayList = new ArrayList<>();
        treeComponentArrayList.add(t1);

        ArrayList<Attachment> attachmentArrayList1 = new ArrayList<>();
        attachmentArrayList.add(a1);

        avviso1.setInteventionList(interventoArrayList);
        avviso1.setTreeComponents(treeComponentArrayList);
        avviso1.setAttachments(attachmentArrayList1);

        availableAvvisi.add(avviso1);
        availableAvvisi.add(avviso1);
        availableAvvisi.add(avviso1);
        availableAvvisi.add(avviso0);
    }

    private void getAvvisiForMonitor() {
        //FAKE DATA
        Avviso avviso0 = new Avviso();
        avviso0.setType("ZD");
        avviso0.setCarriage("654654654");
        try {
            avviso0.setDate(sdf.parse("1/09/2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        avviso0.setCreationImplant("MANTOVA");
        avviso0.setFaultText("FAULT");
        avviso0.setGroup("GROUP");
        avviso0.setFreeText("***");
        avviso0.setInterventionNumber("123123");
        avviso0.setInterventionText("prova");
        avviso0.setLimitation("250 Km/h");
        avviso0.setLongText("long text");
        avviso0.setNumber("999");
        avviso0.setUserState("user state");
        avviso0.setSystemState("system state");
        avviso0.setShortText("short text");
        avviso0.setRotabileSeries("rotabile series");
        avviso0.setWorkOrderNumber("");
        avviso0.setStatus("Impossibile impostare lo stato collaudato");

        Intervento i0 = new Intervento();
        i0.setShortText("short text");
        i0.setCode("code");
        i0.setGroup("group");
        i0.setInterventionText("inter aaa");
        i0.setPosition("position");

        TreeComponent t0 = new TreeComponent();
        t0.setCode("123");
        t0.setDescription("description");

        Attachment a0 = new Attachment();
        a0.setCode("1");
        a0.setDescription(" dest.writeList(treeComponents); dest.writeList(treeComponents); ");

        ArrayList<Intervento> interventoArrayList0 = new ArrayList<>();
        interventoArrayList0.add(i0);

        ArrayList<TreeComponent> treeComponentArrayList0 = new ArrayList<>();
        treeComponentArrayList0.add(t0);

        ArrayList<Attachment> attachmentArrayList = new ArrayList<>();
        attachmentArrayList.add(a0);

        avviso0.setInteventionList(interventoArrayList0);
        avviso0.setTreeComponents(treeComponentArrayList0);
        avviso0.setAttachments(attachmentArrayList);

        Avviso avviso1 = new Avviso();
        avviso1.setType("ZD");
        avviso1.setCarriage("654654654");
        try {
            avviso1.setDate(sdf.parse("17/07/2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        avviso1.setCreationImplant("MANTOVA");
        avviso1.setFaultText("FAULT");
        avviso1.setGroup("GROUP");
        avviso1.setFreeText("***");
        avviso1.setInterventionNumber("123123");
        avviso1.setInterventionText("prova");
        avviso1.setLimitation("250 Km/h");
        avviso1.setLongText("long text");
        avviso1.setNumber("999");
        avviso1.setUserState("user state");
        avviso1.setSystemState("system state");
        avviso1.setShortText("short text");
        avviso1.setRotabileSeries("rotabile series");
        avviso1.setWorkOrderNumber("1");
        avviso1.setStatus("Collaudato");

        Intervento i1 = new Intervento();
        i1.setShortText("short text");
        i1.setCode("code");
        i1.setGroup("group");
        i1.setInterventionText("inter aaa");
        i1.setPosition("position");

        TreeComponent t1 = new TreeComponent();
        t1.setCode("123");
        t1.setDescription("description");

        Attachment a1 = new Attachment();
        a1.setCode("1");
        a1.setDescription(" dest.writeList(treeComponents); dest.writeList(treeComponents); ");

        ArrayList<Intervento> interventoArrayList = new ArrayList<>();
        interventoArrayList.add(i1);

        ArrayList<TreeComponent> treeComponentArrayList = new ArrayList<>();
        treeComponentArrayList.add(t1);

        ArrayList<Attachment> attachmentArrayList1 = new ArrayList<>();
        attachmentArrayList.add(a1);

        avviso1.setInteventionList(interventoArrayList);
        avviso1.setTreeComponents(treeComponentArrayList);
        avviso1.setAttachments(attachmentArrayList);

        Avviso avviso2 = new Avviso();
        avviso2.setType("ZD");
        avviso2.setCarriage("654654654");
        try {
            avviso2.setDate(sdf.parse("31/08/2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        avviso2.setCreationImplant("MANTOVA");
        avviso2.setFaultText("FAULT");
        avviso2.setGroup("GROUP");
        avviso2.setFreeText("***");
        avviso2.setInterventionNumber("123123");
        avviso2.setInterventionText("prova");
        avviso2.setLimitation("250 Km/h");
        avviso2.setLongText("long text");
        avviso2.setNumber("999");
        avviso2.setUserState("user state");
        avviso2.setSystemState("system state");
        avviso2.setShortText("short text");
        avviso2.setRotabileSeries("rotabile series");
        avviso2.setWorkOrderNumber("2");
        avviso2.setStatus("Chiuso");

        Intervento i2 = new Intervento();
        i2.setShortText("short text");
        i2.setCode("code");
        i2.setGroup("group");
        i2.setInterventionText("inter aaa");
        i2.setPosition("position");

        TreeComponent t2 = new TreeComponent();
        t2.setCode("123");
        t2.setDescription("description");

        ArrayList<Intervento> interventoArrayList2 = new ArrayList<>();
        interventoArrayList2.add(i2);

        ArrayList<TreeComponent> treeComponentArrayList2 = new ArrayList<>();
        treeComponentArrayList2.add(t2);

        avviso2.setInteventionList(interventoArrayList2);
        avviso2.setTreeComponents(treeComponentArrayList2);

        availableAvvisi.add(avviso2);
        availableAvvisi.add(avviso1);
        availableAvvisi.add(avviso2);
        availableAvvisi.add(avviso0);
        availableAvvisi.add(avviso1);
        availableAvvisi.add(avviso1);

    }

    public void onAvvisoClick(Avviso avviso) {
        if (avviso != null) {
            Intent intent = new Intent(context, AvvisiDetailActivity.class);
            intent.putExtra(IntentsProvider.AVVISO_ITEM_TO_DETAILAVVISO, avviso);
            intent.putExtra(IntentsProvider.ROTABILE_ITEM_TO_AVVISI, rotabile);
            if (isMonitoraggio)
                intent.putExtra(IntentsProvider.IS_MONITORAGGIO_TO_DETAILAVVISO, isMonitoraggio);
            context.startActivity(intent);
            Log.d(SelectProfileActivity.class.getName(), "Object clicked: " + avviso.toString());
        }
    }
}
