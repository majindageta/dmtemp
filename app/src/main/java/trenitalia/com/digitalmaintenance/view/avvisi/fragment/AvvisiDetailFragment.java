package trenitalia.com.digitalmaintenance.view.avvisi.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.FragmentAvvisiDetailBinding;
import trenitalia.com.digitalmaintenance.view.avvisi.viewModel.AvvisiDetailViewModel;

/**
 * Created by davidericci on 28/08/17.
 */

public class AvvisiDetailFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private AvvisiDetailViewModel viewModel;
    private FragmentAvvisiDetailBinding binding;
    private Boolean isMonitoraggio;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_avvisi_detail, container, false);

        binding.setViewModel(viewModel);
        binding.setVariable(BR.avviso, viewModel.getAvviso());
        viewModel.setFragmentBinding(binding);
        populateLimationsSpinner();
        //TODO handle avviso già bloccato
        if(isMonitoraggio)
            binding.monitoraggioLayout.setVisibility(View.VISIBLE);
        viewModel.enableBlockedActions(false);
        return binding.getRoot();
    }

    public AvvisiDetailViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(AvvisiDetailViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public Boolean getMonitoraggio() {
        return isMonitoraggio;
    }

    public void setMonitoraggio(Boolean monitoraggio) {
        isMonitoraggio = monitoraggio;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    //TODO: click on limitation, what to do
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void populateLimationsSpinner () {
        ArrayList<String> availableLimitations = new ArrayList<>();
        //MOCK
        availableLimitations.add("100 km/h");
        availableLimitations.add("120 km/h");
        availableLimitations.add("140 km/h");
        availableLimitations.add("160 km/h");
        availableLimitations.add("180 km/h");
        //MOCK

        ArrayAdapter<String> limitationAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, availableLimitations);
        limitationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinnerLimitations.setAdapter(limitationAdapter);
        binding.spinnerLimitations.setOnItemSelectedListener(this);
        binding.spinnerLimitations.setSelection(0);
        //TODO
//        if (componenteInConsuntivazione.getStato_() != null && componenteInConsuntivazione.getStato_().length() > 0) {
//            int position = statiCodiciList.indexOf(componenteInConsuntivazione.getStato_());
//            if (position >= 0) {
//                statoCombo.setSelection(position);
//            } else {
//                // FIXME sollevare eccezione
//            }
//        } else {
//            statoCombo.setSelection(0);
//        }
    }

}
