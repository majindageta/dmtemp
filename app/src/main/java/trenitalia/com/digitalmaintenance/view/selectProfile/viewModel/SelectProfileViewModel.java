package trenitalia.com.digitalmaintenance.view.selectProfile.viewModel;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.view.rotabili.RotabiliActivity;
import trenitalia.com.digitalmaintenance.view.selectProfile.SelectProfileActivity;
import trenitalia.com.digitalmaintenance.view.selectProfile.adapter.SelectProfileAdapter;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.datamodel.Implants;
import trenitalia.com.digitalmaintenance.datamodel.Role;

/**
 * Created by davidericci on 24/08/17.
 */

public class SelectProfileViewModel{

    private Context context;
    private SelectProfileAdapter adapter;
    private ArrayList<Implants> availableImplants = new ArrayList<>();
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    public SelectProfileViewModel (Context context) {
        super();

        getImplants();
        this.context = context;
        populateData();
        this.adapter = new SelectProfileAdapter(context, availableImplants, listDataHeader, listDataChild);
    }

    private void populateData () {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        for (Implants imp : availableImplants) {
            listDataHeader.add(imp.getSectionTitle());
            List<String> list = new ArrayList<>();
            for (Role role : imp.getRoles()) {
                list.add(role.getRoleName());
            }
            listDataChild.put(imp.getSectionTitle(), list);
        }
    }

    public void initializeListView(ExpandableListView listView) {
        listView.setAdapter(adapter);
        for (int i = 0; i< listDataHeader.size(); i++) {
            listView.expandGroup(i);
        }
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                expandableListView.expandGroup(i);
                return true;
            }
        });
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Implants implants = availableImplants.get(i);
                Role r = implants.getRoles().get(i1);
                onRoleClick(r, implants);
                return true;
            }
        });
        listView.setGroupIndicator(null);
        listView.setChildIndicator(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            listView.setChildDivider(context.getResources().getDrawable(R.color.white, null));
            listView.setDivider(context.getResources().getDrawable(R.color.white, null));
        } else {
            listView.setChildDivider(context.getResources().getDrawable(R.color.white));
            listView.setDivider(context.getResources().getDrawable(R.color.white));
        }
        listView.setDividerHeight(0);

    }

    public void getImplants() {
        //FAKE DATA
        //roles
        Role r1 = new Role();
        r1.setRoleCode("1");
        r1.setRoleName("Manutentore");
        r1.setEnabled(false);
        Role r2 = new Role();
        r2.setRoleCode("2");
        r2.setRoleName("Capo Tecnico");
        r2.setEnabled(true);
        //Implants
        Implants i1 = new Implants();
        i1.setImplantsName("Impianto Milano");
        i1.setImplantsCode("5563");
        i1.setImplantsLocation("Martesana");
        ArrayList<Role> roles1 = new ArrayList<>();
        roles1.add(r1);
        roles1.add(r2);
        i1.setRoles(roles1);

        Implants i2 = new Implants();
        i2.setImplantsName("Impianto Roma");
        i2.setImplantsCode("8583");
        i2.setImplantsLocation("Prenestina");
        ArrayList<Role> roles2 = new ArrayList<>();
        roles2.add(r1);
        i2.setRoles(roles2);

        availableImplants.add(i1);
        availableImplants.add(i2);
    }

    public ArrayList<Implants> getAvailableImplants() {
        return availableImplants;
    }

    public void setAvailableImplants(ArrayList<Implants> availableImplants) {
        this.availableImplants = availableImplants;
    }

    public void onRoleClick(Role role, Implants implant) {
        if (role != null && implant != null && role.getEnabled()) {
            Intent intent = new Intent(context, RotabiliActivity.class);
            intent.putExtra(IntentsProvider.ROLE_ITEM_TO_ROTABILI, role);
            intent.putExtra(IntentsProvider.IMPLANT_ITEM_TO_ROTABILI, implant);
            context.startActivity(intent);
            Log.d(SelectProfileActivity.class.getName(), "Object clicked: " + role.toString() + " " + implant.toString());
        }
    }
}
