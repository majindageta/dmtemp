package trenitalia.com.digitalmaintenance.view.avvisi;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.datamodel.Avviso;
import trenitalia.com.digitalmaintenance.datamodel.Rotabile;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.view.TabActivity;
import trenitalia.com.digitalmaintenance.view.avvisi.fragment.AttachmentFragment;
import trenitalia.com.digitalmaintenance.view.avvisi.fragment.AvvisiDetailFragment;
import trenitalia.com.digitalmaintenance.view.avvisi.fragment.InterventiFragment;
import trenitalia.com.digitalmaintenance.view.avvisi.fragment.TreeComponentFragment;
import trenitalia.com.digitalmaintenance.view.avvisi.viewModel.AvvisiDetailViewModel;
import trenitalia.com.digitalmaintenance.view.dialog.AvvisoDelayFeedbackDialog;

/**
 * Created by davidericci on 28/08/17.
 */

public class AvvisiDetailActivity extends TabActivity {

    private final String TAG = this.getClass().getSimpleName();
    private AvvisiDetailViewModel viewModel;
    private Rotabile rotabileReceived;
    private Avviso avvisoReceived;
    private Boolean monitoraggio = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromIntent();

        viewModel = new AvvisiDetailViewModel(this, avvisoReceived);
        activityTabBinding.setViewModel(viewModel);
        if (monitoraggio) {
            //TODO: cercare da qualche parte il rotabile (servizio
        }
        activityTabBinding.setVariable(BR.rotabile, rotabileReceived);

        activityTabBinding.setVariable(BR.avviso, avvisoReceived);
        if (monitoraggio)
            activityTabBinding.buttonHandlerAvvisi.setVisibility(View.GONE);

        handleButtons(avvisoReceived.getType(),false); //MOCK TODO: gestire avviso bloccato o no
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras().containsKey(IntentsProvider.ROTABILE_ITEM_TO_AVVISI))
                rotabileReceived = intent.getParcelableExtra(IntentsProvider.ROTABILE_ITEM_TO_AVVISI);
            if (intent.getExtras().containsKey(IntentsProvider.AVVISO_ITEM_TO_DETAILAVVISO))
                avvisoReceived = intent.getParcelableExtra(IntentsProvider.AVVISO_ITEM_TO_DETAILAVVISO);
            if (intent.getExtras().containsKey(IntentsProvider.IS_MONITORAGGIO_TO_DETAILAVVISO))
                monitoraggio = intent.getBooleanExtra(IntentsProvider.IS_MONITORAGGIO_TO_DETAILAVVISO, false);
        }
    }

    @Override
    protected List<Tab> getTabList() {
        List<Tab> tabs = new ArrayList<>();

        AvvisiDetailFragment avvisiDetailFragment =  new AvvisiDetailFragment();
        avvisiDetailFragment.setViewModel(viewModel);
        avvisiDetailFragment.setMonitoraggio(monitoraggio);
        InterventiFragment interventiFragment = new InterventiFragment();
        interventiFragment.setViewModel(viewModel);
        TreeComponentFragment treeComponentFragment = new TreeComponentFragment();
        treeComponentFragment.setViewModel(viewModel);

        Tab one = new Tab("DETTAGLI", avvisiDetailFragment, IntentsProvider.TAB_TYPE.DETAIL.toString());
        Tab two = new Tab("INTERVENTI", interventiFragment, IntentsProvider.TAB_TYPE.INTERVENTION.toString());
        Tab three = new Tab("COMP. ALBERO", treeComponentFragment, IntentsProvider.TAB_TYPE.TREE.toString());

        tabs.add(one);
        tabs.add(two);
        tabs.add(three);

        if (avvisoReceived.getType().equalsIgnoreCase(IntentsProvider.AVVISI_TYPE.ZS.toString()) || avvisoReceived.getType().equalsIgnoreCase(IntentsProvider.AVVISI_TYPE.ZP.toString())) {
            AttachmentFragment attachmentFragment = new AttachmentFragment();
            attachmentFragment.setViewModel(viewModel);
            Tab four = new Tab("ALLEGATI",attachmentFragment, IntentsProvider.TAB_TYPE.ATTACHMENT.toString());
            tabs.add(four);
        }

        return tabs;
    }

    @Override
    protected void onTabSelected(String tabType) {
        Log.d(TAG, "onTabSelected: " + tabType);
        super.onTabSelected(tabType);
        if (viewModel != null)
            viewModel.setSelectedTabType(tabType);
    }

    //Copy this to do the back action
    public void backAction(View v) {
        finish();
    }

    public void closeAction(View v) {
        //TODO: close avviso and return to list
    }

    public void bloccaAction(View v) {
        handleButtons(avvisoReceived.getType(), true);
        viewModel.enableBlockedActions(true);
        //TODO: block avviso
    }

    public void sbloccaAction(View v) {
        handleButtons(avvisoReceived.getType(), false);
        viewModel.enableBlockedActions(false);
        //TODO: unlock avviso
    }

    public void feedbackAction(View v) {
        AvvisoDelayFeedbackDialog feedbackDialog = new AvvisoDelayFeedbackDialog();
        feedbackDialog.setInteface(new AvvisoDelayFeedbackDialog.IUpdateDelayFeedback() {
            @Override
            public void updateMap(String text) {
                Log.d(TAG, text);
                //DO feedback ACTION WITH STRING
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("title", "Seleziona feedback");
        bundle.putString("button", "COMUNICA");
        ArrayList<String> strings = new ArrayList<>();
        strings.add("CODA TIPO FS");
        strings.add("CODA NO TIPO FS");
        //TODO: prendere da avviso

        bundle.putStringArrayList("list", strings);

        feedbackDialog.setArguments(bundle);
        feedbackDialog.show(getSupportFragmentManager(), "FEEDBACK");
    }

    public void rinviaAction(View v) {
        //open modal view
        AvvisoDelayFeedbackDialog reinviaFragment = new AvvisoDelayFeedbackDialog();
        reinviaFragment.setInteface(new AvvisoDelayFeedbackDialog.IUpdateDelayFeedback() {
            @Override
            public void updateMap(String text) {
                Log.d(TAG, text);
                //DO REINVIA ACTION WITH STRING
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("title", "Seleziona motivazione rinvio");
        bundle.putString("button", "RINVIA");

        ArrayList<String> strings = new ArrayList<>();
        strings.add("attesa");
        strings.add("ind");
        strings.add("te");
        strings.add("mancanza");
        //TODO: prendere da avviso

        bundle.putStringArrayList("list", strings);

        reinviaFragment.setArguments(bundle);
        reinviaFragment.show(getSupportFragmentManager(), "REINVIA");
    }

    public void collaudaAction(View v) {
        //send avviso, finish this activity, refresh avvisi activity
    }

    private void handleButtons(String type, Boolean blocked) {
        hideAllButtons();
        if (blocked) {
            activityTabBinding.sbloccaButton.setVisibility(View.VISIBLE);
            if (type.equalsIgnoreCase("ZA") || type.equalsIgnoreCase("ZB") || type.equalsIgnoreCase("ZD")) {
                activityTabBinding.collaudaButton.setVisibility(View.VISIBLE);
                activityTabBinding.rinviaButton.setVisibility(View.VISIBLE);
            } else if (type.equalsIgnoreCase("ZS")) {
                activityTabBinding.feedbackButton.setVisibility(View.VISIBLE);
            } else if (type.equalsIgnoreCase("ZP") || type.equalsIgnoreCase("ZC")) {
                activityTabBinding.chiusuraButton.setVisibility(View.VISIBLE);
            }
        } else {
            activityTabBinding.bloccaButton.setVisibility(View.VISIBLE);
        }
    }

    private void hideAllButtons() {
        activityTabBinding.bloccaButton.setVisibility(View.GONE);
        activityTabBinding.sbloccaButton.setVisibility(View.GONE);
        activityTabBinding.collaudaButton.setVisibility(View.GONE);
        activityTabBinding.rinviaButton.setVisibility(View.GONE);
        activityTabBinding.chiusuraButton.setVisibility(View.GONE);
        activityTabBinding.feedbackButton.setVisibility(View.GONE);
    }

    @BindingAdapter("setVisibilityForFeedback")
    public static void setVisibility(LinearLayout linearLayout, String type) {
        Context c = linearLayout.getContext();
        int visibility;
        if (c != null && !TextUtils.isEmpty(type)) {
            if (type.equalsIgnoreCase(IntentsProvider.AVVISI_TYPE.ZS.toString())) {
                visibility = View.VISIBLE;
            } else {
                visibility = View.GONE;
            }
            linearLayout.setVisibility(visibility);
        }
    }

    @BindingAdapter("setBackgroundMonitoraggio")
    public static void setBackgroundMonitoraggio(LinearLayout linearLayout, String status) {
        Context c = linearLayout.getContext();
        int backgoundColor;
        if (c != null && status != null ) {
            if (status.length() > 0) {
                if (status.equalsIgnoreCase("Collaudato") || status.equalsIgnoreCase("Chiuso")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        backgoundColor = c.getResources().getColor(R.color.greenBackground, null);
                    } else {
                        backgoundColor = c.getResources().getColor(R.color.greenBackground);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        backgoundColor = c.getResources().getColor(R.color.pinkBackground, null);
                    } else {
                        backgoundColor = c.getResources().getColor(R.color.pinkBackground);
                    }
                }
                linearLayout.setBackgroundColor(backgoundColor);
            }
        }
    }
}
