package trenitalia.com.digitalmaintenance.view.rotabili;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.ActivityRotabiliBinding;
import trenitalia.com.digitalmaintenance.datamodel.Implants;
import trenitalia.com.digitalmaintenance.datamodel.Role;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.view.BaseActivity;
import trenitalia.com.digitalmaintenance.view.monitoraggio.MonitoraggioAvvisiActivity;
import trenitalia.com.digitalmaintenance.view.rotabili.viewModel.RotabiliViewModel;

/**
 * Created by davidericci on 25/08/17.
 */

public class RotabiliActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private RotabiliViewModel viewModel;
    private ActivityRotabiliBinding binding;

    private Implants implantReceived;
    private Role roleReceived;
    private Boolean drawerOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_rotabili);
        viewModel = new RotabiliViewModel(this);
        viewModel.initializeRecyclerView(binding.rotabileListview);
        binding.setViewModel(viewModel);

        getDataFromIntent();
        populateLimationsSpinner();

        binding.setVariable(BR.impRotabile, implantReceived);
        binding.setVariable(BR.cid, "MOCK CID");
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras().containsKey(IntentsProvider.IMPLANT_ITEM_TO_ROTABILI))
                implantReceived = intent.getParcelableExtra(IntentsProvider.IMPLANT_ITEM_TO_ROTABILI);
            if (intent.getExtras().containsKey(IntentsProvider.ROLE_ITEM_TO_ROTABILI))
                roleReceived = intent.getParcelableExtra(IntentsProvider.ROLE_ITEM_TO_ROTABILI);
        }
    }

    private void populateLimationsSpinner () {
        ArrayList<String> availableImplants = new ArrayList<>();
        //MOCK
        availableImplants.add("Milano");
        availableImplants.add("Ancona");
        availableImplants.add("Palermo");
        //MOCK

        ArrayAdapter<String> limitationAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, availableImplants);
        limitationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.implantsSpinner.setAdapter(limitationAdapter);
        binding.implantsSpinner.setOnItemSelectedListener(this);
        binding.implantsSpinner.setSelection(0);
        //TODO
//        if (componenteInConsuntivazione.getStato_() != null && componenteInConsuntivazione.getStato_().length() > 0) {
//            int position = statiCodiciList.indexOf(componenteInConsuntivazione.getStato_());
//            if (position >= 0) {
//                statoCombo.setSelection(position);
//            }
//        } else {
//            statoCombo.setSelection(0);
//        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //TODO: click on limitation, what to do
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void menuAction(View v) {
        if (!binding.drawerLayout.isDrawerOpen(binding.navView)) {
            binding.drawerLayout.openDrawer(Gravity.START);
        } else {
            binding.drawerLayout.closeDrawer(binding.navView);
        }
    }

    public void searchAction(View v) {
        binding.header.menuHeader.setVisibility(View.GONE);
        binding.header.searchHeader.setVisibility(View.VISIBLE);

        binding.header.editHeader.setFocusableInTouchMode(true);
        binding.header.editHeader.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(binding.header.editHeader, InputMethodManager.SHOW_IMPLICIT);

        binding.header.editHeader.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    viewModel.resetFilter();
                } else {
                    viewModel.startSearch(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void backHeaderAction(View v) {
        binding.header.menuHeader.setVisibility(View.VISIBLE);
        binding.header.searchHeader.setVisibility(View.GONE);
        viewModel.resetFilter();
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void drawerAvvisiAction(View v) {
        menuAction(v);
    }

    public void drawerMonitoraggioAction(View v) {
        menuAction(v); //close menu
        Intent intent = new Intent(this, MonitoraggioAvvisiActivity.class);
        this.startActivity(intent);
    }

    @BindingAdapter("setImageByRotabile")
    public static void setImage(ImageView imageView, String rotabileType) {
        Context c = imageView.getContext();
        Drawable image;
        //TODO: set image for rotabileType (da definire)
        if (c != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                image = c.getResources().getDrawable(R.drawable.ic_placeholder_profile, null);
            } else {
                image = c.getResources().getDrawable(R.drawable.ic_placeholder_profile);
            }
            imageView.setImageDrawable(image);
        }
    }
}
