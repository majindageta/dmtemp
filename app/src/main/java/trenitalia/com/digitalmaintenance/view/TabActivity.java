package trenitalia.com.digitalmaintenance.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.List;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.ActivityTabBinding;


/**
 * Created by Domenico Di Caro on 12/07/2017.
 */

public abstract class TabActivity extends BaseActivity {

    protected String currentTabType;
    private List<Tab> tabs;
    protected ActivityTabBinding activityTabBinding;
    private static final String TAG = "TabActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "OnCreate");
        activityTabBinding = DataBindingUtil.setContentView(this, R.layout.activity_tab);
        //activityTabBinding.setAction(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (tabs == null) {

            final TabLayout tabLayout = activityTabBinding.tabLayout;
            tabs = getTabList();

            final TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager(), tabs);
            ViewPager viewPager = activityTabBinding.pager;
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {

                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    setCurrentTabType(position);
                    //currentTabType = tabs.get(position).getTabType();
                    //onTabSelected(currentTabType);
                }
            });
            tabLayout.setupWithViewPager(viewPager, true);
            int indexStart = getStartingTabIndex();
            viewPager.setCurrentItem(indexStart);
            setCurrentTabType(indexStart);
        }
    }


    protected abstract List<Tab> getTabList();

    protected void onTabSelected(String tabType) {
        Log.d("TabActivity", "onTabSelected: " + tabType);
    }

    protected int getStartingTabIndex() {
        return 0;
    }

    private void setCurrentTabType(int position) {
        currentTabType = tabs.get(position).getTabType();
        onTabSelected(currentTabType);
    }

    public static class Tab {

        //private int icon;
        private String tabType;
        private String tabTitle;
        private Fragment fragment;

        public Tab( String tabTitle, Fragment fragment, String tabType) {
            this.tabType = tabType;

            this.tabTitle = tabTitle;
            this.fragment = fragment;
        }

        public String getTabType() {
            return tabType;
        }

        public String getTabTitle() {
            return tabTitle;
        }

        public Fragment getFragment() {
            return fragment;
        }
    }


    public static class TabPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Tab> tabs;

        public TabPagerAdapter(FragmentManager fm, List<Tab> tabs) {
            super(fm);
            this.tabs = tabs;
        }

        @Override
        public Fragment getItem(int position) {

            if (tabs != null) {
                return tabs.get(position).getFragment();
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return tabs != null ? tabs.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs.get(position).getTabTitle();
        }
    }
}
