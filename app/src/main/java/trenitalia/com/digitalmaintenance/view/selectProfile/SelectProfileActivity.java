package trenitalia.com.digitalmaintenance.view.selectProfile;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.view.BaseActivity;
import trenitalia.com.digitalmaintenance.view.selectProfile.viewModel.SelectProfileViewModel;
import trenitalia.com.digitalmaintenance.databinding.ActivitySelectProfileBinding;

public class SelectProfileActivity extends BaseActivity {

    private ActivitySelectProfileBinding binding;
    private SelectProfileViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_profile);
        viewModel = new SelectProfileViewModel(this);
        binding.setViewModel(viewModel);

        viewModel.initializeListView(binding.selectListview);
    }

    //Copy this to do the back action
    public void backAction(View v) {
        finish();
    }

    public SelectProfileViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(SelectProfileViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @BindingAdapter("setImageByRole")
    public static void setImage(ImageView imageView, Boolean enabled) {
        Context c = imageView.getContext();
        Drawable image;
        if (c != null ) {
            if (enabled) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    image = c.getResources().getDrawable(R.drawable.ic_placeholder_profile, null);
                } else {
                    image = c.getResources().getDrawable(R.drawable.ic_placeholder_profile);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    image = c.getResources().getDrawable(R.drawable.ic_logo_trenitalia, null);
                } else {
                    image = c.getResources().getDrawable(R.drawable.ic_logo_trenitalia);
                }
            }
            imageView.setImageDrawable(image);
        }
    }
}
