package trenitalia.com.digitalmaintenance.view.avvisi.adapter;

import android.databinding.ViewDataBinding;


import java.util.List;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.interfaces.RecyclerViewBaseAdapter;

/**
 * Created by davidericci on 29/08/17.
 */

public class InterventiAdapter  extends RecyclerViewBaseAdapter {

    private List<?> interventi;

    public InterventiAdapter(int nameLayout) {
        super(nameLayout);
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        binding.setVariable(BR.intervento, interventi.get(position));
    }

    @Override
    public void setDataSource(List<?> dataSource) {
        this.interventi = dataSource;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return interventi != null ? interventi.size() : 0;
    }
}
