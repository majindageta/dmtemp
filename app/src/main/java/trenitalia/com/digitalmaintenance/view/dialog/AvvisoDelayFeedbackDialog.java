package trenitalia.com.digitalmaintenance.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Created by davidericci on 29/08/17.
 */

public class AvvisoDelayFeedbackDialog extends android.support.v4.app.DialogFragment {

    private String selectedChoice;
    private Boolean isDelay = false;
    private IUpdateDelayFeedback inteface;
    private AlertDialog dialog = null;

    public void setInteface(IUpdateDelayFeedback inteface) {
        this.inteface = inteface;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        String title = bundle.getString("title");
        final ArrayList<String> list = bundle.getStringArrayList("list");
        String buttonOK = bundle.getString("button");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (!list.isEmpty()) {
            CharSequence[] charSequences = new CharSequence[list.size()];
            for (int i = 0; i < list.size(); i++) {
                String t = list.get(i);
                charSequences[i] = t;
            }

            builder.setTitle(title)
                    .setSingleChoiceItems(charSequences, -1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    selectedChoice = list.get(i);
                                    if (dialog != null)
                                        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                                .setEnabled(true);
                                }
                            }
                    )
                    .setPositiveButton(buttonOK, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (selectedChoice != null && selectedChoice.length() > 0) {
                                if (inteface != null)
                                    inteface.updateMap(selectedChoice);
                            }
                        }
                    })
                    .setCancelable(false)
                    .setNegativeButton("ANNULLA", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
         }
        dialog = builder.create();
        dialog.show();
        if (dialog != null && dialog.getButton(AlertDialog.BUTTON_POSITIVE) != null)
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        return dialog;
    }

    public interface IUpdateDelayFeedback {
        void updateMap(String text);
    }

}
