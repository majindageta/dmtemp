package trenitalia.com.digitalmaintenance.view.rotabili.viewModel;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

import trenitalia.com.digitalmaintenance.interfaces.OnItemRecyclerClickListener;
import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.view.avvisi.AvvisiActivity;
import trenitalia.com.digitalmaintenance.view.rotabili.RotabiliActivity;
import trenitalia.com.digitalmaintenance.view.rotabili.adapter.RotabiliAdapter;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.datamodel.Rotabile;

/**
 * Created by davidericci on 25/08/17.
 */

public class RotabiliViewModel implements
        OnItemRecyclerClickListener<Rotabile> {

    private Context context;
    private RotabiliAdapter adapter;
    private ArrayList<Rotabile> rotabileArrayList = new ArrayList<>();
    private ArrayList<Rotabile> filteredRotabileArrayList = new ArrayList<>();
    public RotabiliViewModel(@NonNull Context context) {
        this.context = context;
        this.adapter = new RotabiliAdapter(R.layout.rotabili_list_item, this);
    }

    public void initializeRecyclerView(RecyclerView recyclerView) {

        initializeData();
        filteredRotabileArrayList.addAll(rotabileArrayList);

        adapter.setDataSource(filteredRotabileArrayList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(Rotabile variable) {
        if (variable != null) {
            Intent intent = new Intent(context, AvvisiActivity.class);
            intent.putExtra(IntentsProvider.ROTABILE_ITEM_TO_AVVISI, variable);
            context.startActivity(intent);
            Log.d(RotabiliActivity.class.getName(), "Object clicked: " + variable.toString());
        }
    }

    private void initializeData () {
        Rotabile rotabile1 = new Rotabile();
        rotabile1.setNumber("220");
        rotabile1.setRail("Bin. 1");
        rotabile1.setLastExit(38);
        rotabile1.setLocation("Gianturco");
        rotabile1.setSerial("VE401A");
        rotabile1.setType("type1");

        Rotabile rotabile2 = new Rotabile();
        rotabile2.setNumber("AV9819");
        rotabile2.setRail("Bin. 2");
        rotabile2.setLastExit(12);
        rotabile2.setLocation("Garibaldi");
        rotabile2.setSerial("ETR10000002");
        rotabile2.setType("type2");

        Rotabile rotabile3 = new Rotabile();
        rotabile3.setNumber("220");
        rotabile3.setRail("Bin. 1");
        rotabile3.setLastExit(38);
        rotabile3.setLocation("Gianturco");
        rotabile3.setSerial("VI401A");
        rotabile3.setType("type1");

        Rotabile rotabile4 = new Rotabile();
        rotabile4.setNumber("AT9819");
        rotabile4.setRail("Bin. 2");
        rotabile4.setLastExit(12);
        rotabile4.setLocation("Garibaldi");
        rotabile4.setSerial("ETP10000002");
        rotabile4.setType("type2");

        rotabileArrayList.add(rotabile1);
        rotabileArrayList.add(rotabile2);
        rotabileArrayList.add(rotabile3);
        rotabileArrayList.add(rotabile4);
    }

    public void startSearch(String searchParameter) {
        adapter.getFilter().filter(searchParameter);
    }
    public void resetFilter () {
        adapter.setDataSource(rotabileArrayList);
    }
}
