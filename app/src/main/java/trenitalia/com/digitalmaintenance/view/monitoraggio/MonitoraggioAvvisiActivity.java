package trenitalia.com.digitalmaintenance.view.monitoraggio;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.ActivityMonitoraggioAvvisiBinding;
import trenitalia.com.digitalmaintenance.view.BaseActivity;
import trenitalia.com.digitalmaintenance.view.avvisi.viewModel.AvvisiViewModel;

/**
 * Created by ss on 29/08/17.
 */

public class MonitoraggioAvvisiActivity extends BaseActivity {

    private ActivityMonitoraggioAvvisiBinding binding;
    private AvvisiViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_monitoraggio_avvisi);
        viewModel = new AvvisiViewModel(this, binding);
        binding.setViewModel(viewModel);
        viewModel.initializeListView(binding.monitoraggioListview, true);
    }

    //Copy this to do the back action
    public void backAction(View v) {
        finish();
    }

    public AvvisiViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(AvvisiViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @BindingAdapter("setBackgroundAndTextColorMonitoraggio")
    public static void setBackgroundAndTextColorByType(TextView textView, String workOrderNumber) {
        Context c = textView.getContext();
        int backgoundColor;
        int textColor;
        if (c != null && workOrderNumber != null ) {
            if (workOrderNumber.length() > 0) {
                if (workOrderNumber.equalsIgnoreCase("Collaudato") || workOrderNumber.equalsIgnoreCase("Chiuso")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        backgoundColor = c.getResources().getColor(R.color.greenBackground, null);
                        textColor = c.getResources().getColor(R.color.greenText, null);
                    } else {
                        backgoundColor = c.getResources().getColor(R.color.greenBackground);
                        textColor = c.getResources().getColor(R.color.greenText);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        backgoundColor = c.getResources().getColor(R.color.pinkBackground, null);
                        textColor = c.getResources().getColor(R.color.redText, null);
                    } else {
                        backgoundColor = c.getResources().getColor(R.color.pinkBackground);
                        textColor = c.getResources().getColor(R.color.redText);
                    }
                }
                textView.setTextColor(textColor);
                textView.setBackgroundColor(backgoundColor);
                textView.setText(workOrderNumber);
            }
        }
    }
}
