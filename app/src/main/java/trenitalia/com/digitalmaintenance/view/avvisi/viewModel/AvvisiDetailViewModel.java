package trenitalia.com.digitalmaintenance.view.avvisi.viewModel;

import android.content.Context;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.FragmentAttachmentBinding;
import trenitalia.com.digitalmaintenance.databinding.FragmentAvvisiDetailBinding;
import trenitalia.com.digitalmaintenance.databinding.FragmentInterventiBinding;
import trenitalia.com.digitalmaintenance.databinding.FragmentTreeComponentBinding;
import trenitalia.com.digitalmaintenance.datamodel.Avviso;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.view.TabActivityViewModel;
import trenitalia.com.digitalmaintenance.view.avvisi.adapter.AttachmentAdapter;
import trenitalia.com.digitalmaintenance.view.avvisi.adapter.InterventiAdapter;
import trenitalia.com.digitalmaintenance.view.avvisi.adapter.TreeComponentAdapter;

/**
 * Created by davidericci on 28/08/17.
 */

public class AvvisiDetailViewModel extends TabActivityViewModel {

    private Context context;
    private Avviso avviso;
    private InterventiAdapter interventiAdapter;
    private TreeComponentAdapter treeComponentAdapter;
    private AttachmentAdapter attachmentAdapter;
    private FragmentAvvisiDetailBinding fragmentBinding;

    private FragmentInterventiBinding fragmentInterventiBinding;
    private FragmentTreeComponentBinding treeComponentBinding;
    private FragmentAttachmentBinding fragmentAttachmentBinding;

    public AvvisiDetailViewModel(@NonNull Context context, Avviso avviso) {
        this.context = context;
        this.avviso = avviso;

        this.interventiAdapter = new InterventiAdapter(R.layout.interventi_list_item);
        this.treeComponentAdapter = new TreeComponentAdapter(R.layout.tree_component_list_item);
        this.attachmentAdapter = new AttachmentAdapter(R.layout.attachment_list_item);
    }

    @Bindable
    @Override
    public String getSelectedTabType() {
        return selectedTabType;
    }

    @Override
    public void setSelectedTabType(String tabType) {

        Log.d("AvvisiDetailViewModel", "setSelectedTabType: " + tabType);
        this.selectedTabType = tabType;
//        notifyPropertyChanged(BR.selectedTabType);

//        if (tabType.equalsIgnoreCase(Collections.TAB_TYPE.DATA_NOT_SEND.toString())) {
//            adapter.setDataSource(monitorDocumentsNotRegistered);
//            setMonitorNumberData(String.format(context.getResources().getString(R.string.monitor_result_number_not_sent), monitorDocumentsNotRegistered.size() + ""));
//        } else {
//            setMonitorNumberData(String.format(context.getResources().getString(R.string.monitor_result_number_sent), monitorDocumentsRegistered.size()+""));
//            adapter.setDataSource(monitorDocumentsRegistered);
//        }
    }

    public void initializeRecyclerView(RecyclerView recyclerView, IntentsProvider.TAB_TYPE type) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        switch (type) {
            case INTERVENTION:
                interventiAdapter.setDataSource(avviso.getInteventionList());
                recyclerView.setAdapter(interventiAdapter);
                break;
            case ATTACHMENT:
                attachmentAdapter.setDataSource(avviso.getAttachments());
                recyclerView.setAdapter(attachmentAdapter);
                break;
            case DETAIL:
                break;
            case TREE:
                treeComponentAdapter.setDataSource(avviso.getTreeComponents());
                recyclerView.setAdapter(treeComponentAdapter);
                break;
            default:
                break;
        }
    }

    public Avviso getAvviso() {
        return avviso;
    }

    public void setAvviso(Avviso avviso) {
        this.avviso = avviso;
    }

    public FragmentAvvisiDetailBinding getFragmentBinding() {
        return fragmentBinding;
    }

    public void setFragmentBinding(FragmentAvvisiDetailBinding fragmentBinding) {
        this.fragmentBinding = fragmentBinding;
    }

    public void setFragmentBinding(FragmentInterventiBinding fragmentBinding) {
        this.fragmentInterventiBinding = fragmentBinding;
    }

    public void setFragmentBinding (FragmentTreeComponentBinding fragmentBinding) {
        this.treeComponentBinding = fragmentBinding;
    }

    public void setFragmentBinding (FragmentAttachmentBinding fragmentBinding) {
        this.fragmentAttachmentBinding = fragmentBinding;
    }

    public void enableBlockedActions(Boolean enable) {
        fragmentBinding.longEditText.setEnabled(enable);
        fragmentBinding.spinnerLimitations.setEnabled(enable);
        fragmentBinding.spinnerLimitations.setClickable(enable);
    }

}
