package trenitalia.com.digitalmaintenance.view.rotabili.adapter;

import android.databinding.ViewDataBinding;
import android.util.Log;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.datamodel.Rotabile;
import trenitalia.com.digitalmaintenance.interfaces.OnItemRecyclerClickListener;
import trenitalia.com.digitalmaintenance.interfaces.RecyclerViewBaseAdapter;

/**
 * Created by davidericci on 25/08/17.
 */

public class RotabiliAdapter extends RecyclerViewBaseAdapter implements Filterable{

    private List<?> rotabili;
    private List rotabiliFiltered = new ArrayList();
    private Filter mFilter;
    private OnItemRecyclerClickListener listener;

    public RotabiliAdapter(int nameLayout, OnItemRecyclerClickListener<Rotabile> listener) {
        super(nameLayout);
        this.listener = listener;
        mFilter = new CustomFilter(RotabiliAdapter.this);
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        binding.setVariable(BR.rotabile, rotabiliFiltered.get(position));
        binding.setVariable(BR.clickListener, listener);
    }

    @Override
    public void setDataSource(List<?> dataSource) {
        this.rotabili = dataSource;
        if (this.rotabiliFiltered != null) this.rotabiliFiltered.clear();
        this.rotabiliFiltered.addAll(rotabili);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rotabiliFiltered != null ? rotabiliFiltered.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class CustomFilter extends Filter {
        private RotabiliAdapter mAdapter;
        private CustomFilter(RotabiliAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            rotabiliFiltered.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                rotabiliFiltered.addAll(rotabili);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Object mWords : rotabili) {
                    Rotabile rotabile = (Rotabile) mWords;
                    if (rotabile.getSerial().toLowerCase().startsWith(filterPattern)) {
                        rotabiliFiltered.add(mWords);
                    } else if (rotabile.getNumber().toLowerCase().startsWith(filterPattern)) {
                        rotabiliFiltered.add(mWords);
                    }
                }
            }
            Log.d("TAG", "Count Number " + rotabiliFiltered.size());
            results.values = rotabiliFiltered;
            results.count = rotabiliFiltered.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null)
                Log.d("TAG","Count Number 2 " + ((List<Rotabile>) results.values).size());
            this.mAdapter.notifyDataSetChanged();
        }
    }
}
