package trenitalia.com.digitalmaintenance.view.avvisi.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.FragmentAttachmentBinding;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.view.avvisi.viewModel.AvvisiDetailViewModel;

/**
 * Created by davidericci on 29/08/17.
 */

public class AttachmentFragment extends Fragment {

    private AvvisiDetailViewModel viewModel;
    private FragmentAttachmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_attachment, container, false);

        binding.setViewModel(viewModel);
       viewModel.setFragmentBinding(binding);
        viewModel.initializeRecyclerView(binding.attachmentRecycleView, IntentsProvider.TAB_TYPE.ATTACHMENT); //true if is from Interventi
        return binding.getRoot();
    }

    public AvvisiDetailViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(AvvisiDetailViewModel viewModel) {
        this.viewModel = viewModel;
    }
}
