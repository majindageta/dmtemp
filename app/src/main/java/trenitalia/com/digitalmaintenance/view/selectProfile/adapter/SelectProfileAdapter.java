package trenitalia.com.digitalmaintenance.view.selectProfile.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.SelectProfileListItemBinding;
import trenitalia.com.digitalmaintenance.databinding.SelectRoleListItemBinding;
import trenitalia.com.digitalmaintenance.datamodel.Implants;

/**
 * Created by davidericci on 24/08/17.
 */

public class SelectProfileAdapter extends BaseExpandableListAdapter {
    private ArrayList<Implants> implants;
    private Context context;

    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public SelectProfileAdapter(Context context, ArrayList<Implants> implants, List<String> listDataHeader, HashMap<String, List<String>> listDataChild) {
        super();
        this.context = context;
        this.implants = implants;
        _listDataHeader = listDataHeader;
        _listDataChild = listDataChild;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        SelectRoleListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_role_list_item, null, true);
        binding.setRole(implants.get(groupPosition).getRoles().get(childPosition));
        return binding.getRoot();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        SelectProfileListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_profile_list_item, null, true);
        binding.setImplants(implants.get(groupPosition));
        return binding.getRoot();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
