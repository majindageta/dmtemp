package trenitalia.com.digitalmaintenance.view;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;

/**
 * Created by Domenico Di Caro on 17/07/2017.
 */

public abstract class TabActivityViewModel extends BaseObservable {

    public ObservableField<String> title = new ObservableField<>();

    protected String selectedTabType;

    public abstract String getSelectedTabType();

    public abstract void setSelectedTabType(String tabType);

    //public ObservableBoolean isLoadingVisible = new ObservableBoolean();
}
