package trenitalia.com.digitalmaintenance.view.avvisi.adapter;

import android.databinding.ViewDataBinding;

import java.util.List;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.interfaces.RecyclerViewBaseAdapter;

/**
 * Created by davidericci on 29/08/17.
 */

public class AttachmentAdapter extends RecyclerViewBaseAdapter {

    private List<?> attachment;

    public AttachmentAdapter(int nameLayout) {
        super(nameLayout);
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        binding.setVariable(BR.attachment, attachment.get(position));
    }

    @Override
    public void setDataSource(List<?> dataSource) {
        this.attachment = dataSource;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return attachment != null ? attachment.size() : 0;
    }
}