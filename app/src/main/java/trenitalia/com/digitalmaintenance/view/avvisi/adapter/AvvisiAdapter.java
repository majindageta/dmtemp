package trenitalia.com.digitalmaintenance.view.avvisi.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.databinding.AvvisiChildItemBinding;
import trenitalia.com.digitalmaintenance.databinding.AvvisiParentItemBinding;
import trenitalia.com.digitalmaintenance.databinding.MonitoraggioChildItemBinding;
import trenitalia.com.digitalmaintenance.datamodel.Avviso;

/**
 * Created by davidericci on 25/08/17.
 */

public class AvvisiAdapter extends BaseExpandableListAdapter {
    private ArrayList<Avviso> avvisi;
    private Context context;

    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Avviso>> _listDataChild;
    private boolean monitoraggio;
    private SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyyMMdd", Locale.ITALIAN);
    SimpleDateFormat outputFormatter = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.ITALIAN);

    public AvvisiAdapter(Context context, ArrayList<Avviso> avvisi, List<String> listDataHeader, HashMap<String, List<Avviso>> listDataChild, Boolean isMonitoraggio) {
        super();
        this.context = context;
        this.avvisi = avvisi;
        this.monitoraggio = isMonitoraggio;
        _listDataHeader = listDataHeader;
        _listDataChild = listDataChild;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (monitoraggio) {
            MonitoraggioChildItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.monitoraggio_child_item, null, true);
            String key = _listDataHeader.get(groupPosition);
            Avviso value = _listDataChild.get(key).get(childPosition);
            binding.setAvviso(value);
            return binding.getRoot();
        } else {
            AvvisiChildItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.avvisi_child_item, null, true);
            String key = _listDataHeader.get(groupPosition);
            Avviso value = _listDataChild.get(key).get(childPosition);
            binding.setAvviso(value);
            return binding.getRoot();
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        AvvisiParentItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.avvisi_parent_item, null, true);
        String key = _listDataHeader.get(groupPosition);
        if (monitoraggio) {
            try {
                Date date = inputFormatter.parse(key);
                key = getTodayOrYesterday(date.getTime()) + outputFormatter.format(date);
            } catch (ParseException e) {
                key = _listDataHeader.get(groupPosition);
            }
        } else {
            if (key != null && key.length() == 0)
                key = "Avvisi non associati a ODL"; //Only for avviso, not monitoraggio
        }
        binding.setWorkOrder(key);
        return binding.getRoot();
    }

    private String getTodayOrYesterday(long smsTimeInMilis) {
        if (DateUtils.isToday(smsTimeInMilis)) {
            return "Oggi - ";
        }
        if (DateUtils.isToday(smsTimeInMilis + ((24 * 60 * 60) * 1000))) {
            return "Ieri - ";
        }
        return "";
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
