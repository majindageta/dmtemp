package trenitalia.com.digitalmaintenance.view.avvisi.adapter;

import android.databinding.ViewDataBinding;

import java.util.List;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.interfaces.RecyclerViewBaseAdapter;

/**
 * Created by davidericci on 29/08/17.
 */

public class TreeComponentAdapter extends RecyclerViewBaseAdapter {

    private List<?> treeComponent;

    public TreeComponentAdapter(int nameLayout) {
        super(nameLayout);
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        binding.setVariable(BR.tree, treeComponent.get(position));
    }

    @Override
    public void setDataSource(List<?> dataSource) {
        this.treeComponent = dataSource;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return treeComponent != null ? treeComponent.size() : 0;
    }
}