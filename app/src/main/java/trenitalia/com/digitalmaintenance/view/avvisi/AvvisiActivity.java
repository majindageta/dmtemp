package trenitalia.com.digitalmaintenance.view.avvisi;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import trenitalia.com.digitalmaintenance.BR;
import trenitalia.com.digitalmaintenance.R;
import trenitalia.com.digitalmaintenance.view.BaseActivity;
import trenitalia.com.digitalmaintenance.view.avvisi.viewModel.AvvisiViewModel;
import trenitalia.com.digitalmaintenance.utils.IntentsProvider;
import trenitalia.com.digitalmaintenance.databinding.ActivityAvvisiBinding;
import trenitalia.com.digitalmaintenance.datamodel.Rotabile;

/**
 * Created by davidericci on 25/08/17.
 */

public class AvvisiActivity extends BaseActivity {

    private ActivityAvvisiBinding binding;
    private AvvisiViewModel viewModel;
    private Rotabile rotabileReceived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromIntent();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_avvisi);
        viewModel = new AvvisiViewModel(this, binding, rotabileReceived);
        binding.setViewModel(viewModel);

        viewModel.initializeListView(binding.avvisiListview, false);
        binding.setVariable(BR.rotabile, rotabileReceived);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras().containsKey(IntentsProvider.ROTABILE_ITEM_TO_AVVISI))
                rotabileReceived = intent.getParcelableExtra(IntentsProvider.ROTABILE_ITEM_TO_AVVISI);
        }
    }

    //Copy this to do the back action
    public void backAction(View v) {
        finish();
    }

    public AvvisiViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(AvvisiViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @BindingAdapter("setBackgroundAndTextColorByType")
    public static void setBackgroundAndTextColorByType(TextView textView, String workOrderNumber) {
        Context c = textView.getContext();
        int backgoundColor;
        int textColor;
        String text;
        if (c != null && workOrderNumber != null ) {
            if (workOrderNumber.length() > 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    backgoundColor = c.getResources().getColor(R.color.yellowBackground, null);
                    textColor = c.getResources().getColor(R.color.yellowText, null);
                } else {
                    backgoundColor = c.getResources().getColor(R.color.yellowBackground);
                    textColor = c.getResources().getColor(R.color.yellowText);
                }
                text = " Aperto ";
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    backgoundColor = c.getResources().getColor(R.color.pinkBackground, null);
                    textColor = c.getResources().getColor(R.color.redText, null);
                } else {
                    backgoundColor = c.getResources().getColor(R.color.pinkBackground);
                    textColor = c.getResources().getColor(R.color.redText);
                }
                text = " Da associare a ODL ";
            }
            textView.setTextColor(textColor);
            textView.setBackgroundColor(backgoundColor);
            textView.setText(text);
        }
    }
}
