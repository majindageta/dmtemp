package trenitalia.com.digitalmaintenance;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by domenico.dicaro on 21/07/2017.
 */

public class LineDividerDecoration extends RecyclerView.ItemDecoration {

    private final Drawable divider;

    public LineDividerDecoration(Context context) {
        divider = getDividerDrawable(context);
    }

    private Drawable getDividerDrawable(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(R.drawable.list_divider_gray);
        } else {
            return context.getResources().getDrawable(R.drawable.list_divider_gray, null);
        }
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
        final int childCount = parent.getChildCount();

        for (int i = 1; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int size = divider.getIntrinsicHeight();

            int top = child.getTop() - params.topMargin;
            int bottom = top + size;
            divider.setBounds(left, top, right, bottom);
            divider.draw(c);
        }
    }
}
