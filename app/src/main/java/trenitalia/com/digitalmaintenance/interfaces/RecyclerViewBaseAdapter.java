package trenitalia.com.digitalmaintenance.interfaces;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Domenico Di Caro on 19/07/2017.
 */

public abstract class RecyclerViewBaseAdapter extends RecyclerView.Adapter<RecyclerViewBaseAdapter.CustomViewHolder> {

    private int layoutID;

    /**
     * Constructor base adapter recycler view.
     *
     * @param nameLayout layoutID of single row
     */
    public RecyclerViewBaseAdapter(int nameLayout) {
        this.layoutID = nameLayout;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding mLayoutBinding;

        CustomViewHolder(ViewDataBinding layoutBinding) {
            super(layoutBinding.getRoot());
            this.mLayoutBinding = layoutBinding;
        }

        public ViewDataBinding getBinding() {
            return mLayoutBinding;
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, layoutID, parent, false);
        return new CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        ViewDataBinding binding = holder.getBinding();
        updateBinding(binding, position);
        binding.executePendingBindings();
    }

    protected abstract void updateBinding(ViewDataBinding binding, int position);

    public abstract void setDataSource(List<?> dataSource);
}