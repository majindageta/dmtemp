package trenitalia.com.digitalmaintenance.interfaces;

/**
 * Created by domenico.dicaro on 21/07/2017.
 */

public interface OnItemRecyclerClickListener<T> {
    void onItemClick(T variable);
}
