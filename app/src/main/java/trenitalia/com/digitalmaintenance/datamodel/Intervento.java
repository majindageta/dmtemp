package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by davidericci on 25/08/17.
 */

public class Intervento implements Parcelable {

    private String code;
    private String position;
    private String group;
    private String shortText;
    private String interventionText;

    public Intervento() {
    }

    public static final Creator<Intervento> CREATOR = new Creator<Intervento>() {
        @Override
        public Intervento createFromParcel(Parcel in) {
            return new Intervento(in);
        }

        @Override
        public Intervento[] newArray(int size) {
            return new Intervento[size];
        }
    };

    protected Intervento(Parcel in) {
        code= in.readString();
        position = in.readString();
        group = in.readString();
        shortText = in.readString();
        interventionText = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(position);
        dest.writeString(group);
        dest.writeString(shortText);
        dest.writeString(interventionText);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getInterventionText() {
        return interventionText;
    }

    public void setInterventionText(String interventionText) {
        this.interventionText = interventionText;
    }
}
