package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by davidericci on 25/08/17.
 */

public class Rotabile implements Parcelable{
    private String type;
    private String serial;
    private String number;
    private long lastExit;
    private String location;
    private String rail;

    private String rotabilePosition;
    private String rotabileExit;
    private String rotabileTitle;

    public Rotabile() {}

    public static final Creator<Rotabile> CREATOR = new Creator<Rotabile>() {
        @Override
        public Rotabile createFromParcel(Parcel in) {
            return new Rotabile(in);
        }

        @Override
        public Rotabile[] newArray(int size) {
            return new Rotabile[size];
        }
    };

    protected Rotabile(Parcel in) {
        type = in.readString();
        serial = in.readString();
        number = in.readString();
        lastExit = in.readLong();
        location = in.readString();
        rail = in.readString();

        rotabilePosition = in.readString();
        rotabileExit = in.readString();
        rotabileTitle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(serial);
        dest.writeString(number);
        dest.writeLong(lastExit);
        dest.writeString(location);
        dest.writeString(rail);

        dest.writeString(rotabilePosition);
        dest.writeString(rotabileExit);
        dest.writeString(rotabileTitle);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        setRotabileStrings();
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
        setRotabileStrings();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
        setRotabileStrings();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
        setRotabileStrings();
    }

    public String getRail() {
        return rail;
    }

    public void setRail(String rail) {
        this.rail = rail;
        setRotabileStrings();
    }

    public long getLastExit() {
        return lastExit;
    }

    public void setLastExit(long lastExit) {
        this.lastExit = lastExit;
        setRotabileStrings();
    }

    public String getRotabilePosition() {
        return rotabilePosition;
    }

    public void setRotabilePosition(String rotabilePosition) {
        this.rotabilePosition = rotabilePosition;
    }

    public String getRotabileExit() {
        return rotabileExit;
    }

    public void setRotabileExit(String rotabileExit) {
        this.rotabileExit = rotabileExit;
    }

    public String getRotabileTitle() {
        return rotabileTitle;
    }

    public void setRotabileTitle(String rotabileTitle) {
        this.rotabileTitle = rotabileTitle;
    }

    public void setRotabileStrings() {
        setRotabilePosition(location + " - " + rail);
        setRotabileTitle(serial + " / " + number);
        setRotabileExit("Uscita prevista: " + lastExit + "min");
    }
}
