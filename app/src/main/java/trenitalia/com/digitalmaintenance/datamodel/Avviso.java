package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by davidericci on 25/08/17.
 */

public class Avviso implements Parcelable {

    private String number;
    private String shortText;
    private String type;
    private String workOrderNumber;
    private String carriage;
    private String longText;
    private String creationDate;
    private String interventionNumber;
    private String creationImplant;
    private String systemState;
    private String userState;
    private String interventionText;
    private String faultText;
    private String freeText;
    private String group;
    private String rotabileSeries;
    private String limitation;
    private String feedback;
    private int blocked;
    private String avvisiChildText;
    private String status;
    private Date date;

    private ArrayList<Intervento> inteventionList;
    private ArrayList<TreeComponent> treeComponents;
    private ArrayList<Attachment> attachments;

    public Avviso() {

    }

    public static final Creator<Avviso> CREATOR = new Creator<Avviso>() {
        @Override
        public Avviso createFromParcel(Parcel in) {
            return new Avviso(in);
        }

        @Override
        public Avviso[] newArray(int size) {
            return new Avviso[size];
        }
    };

    protected Avviso(Parcel in) {
        number= in.readString();
        shortText = in.readString();
        type = in.readString();
        workOrderNumber = in.readString();
        carriage = in.readString();
        longText = in.readString();
        creationDate = in.readString();
        interventionNumber = in.readString();
        creationImplant = in.readString();
        systemState = in.readString();
        userState = in.readString();
        interventionText = in.readString();
        faultText = in.readString();
        freeText = in.readString();
        group = in.readString();
        rotabileSeries = in.readString();
        limitation = in.readString();
        avvisiChildText = in.readString();
        blocked = in.readInt();
        feedback = in.readString();
        status = in.readString();

        inteventionList = in.readArrayList(Intervento.class.getClassLoader());
        treeComponents = in.readArrayList(TreeComponent.class.getClassLoader());
        attachments = in.readArrayList(Attachment.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeString(shortText);
        dest.writeString(type);
        dest.writeString(workOrderNumber);
        dest.writeString(carriage);
        dest.writeString(longText);
        dest.writeString(creationDate);
        dest.writeString(interventionNumber);
        dest.writeString(creationImplant);
        dest.writeString(systemState);
        dest.writeString(userState);
        dest.writeString(interventionText);
        dest.writeString(faultText);
        dest.writeString(freeText);
        dest.writeString(group);
        dest.writeString(rotabileSeries);
        dest.writeString(limitation);
        dest.writeString(avvisiChildText);
        dest.writeInt(blocked);
        dest.writeString(feedback);
        dest.writeString(status);

        dest.writeList(inteventionList);
        dest.writeList(treeComponents);
        dest.writeList(attachments);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
        setAvvisiChildText();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        setAvvisiChildText();
    }

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

    public String getCarriage() {
        return carriage;
    }

    public void setCarriage(String carriage) {
        this.carriage = carriage;
    }

    public String getLongText() {
        return longText;
    }

    public void setLongText(String longText) {
        this.longText = longText;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate() {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ITALIAN);
            this.creationDate = sdf.format(date);
        } else {
            this.creationDate = "NO DATE";
        }
    }
//    private String getTodayOrYesterday(long smsTimeInMilis) {
//        if (DateUtils.isToday(smsTimeInMilis)) {
//            return "Oggi - ";
//        }
//        if (DateUtils.isToday(smsTimeInMilis + ((24*60*60) * 1000))) {
//            return "Ieri - ";
//        }
//        return "";
//    }

    public String getInterventionNumber() {
        return interventionNumber;
    }

    public void setInterventionNumber(String interventionNumber) {
        this.interventionNumber = interventionNumber;
    }

    public String getCreationImplant() {
        return creationImplant;
    }

    public void setCreationImplant(String creationImplant) {
        this.creationImplant = creationImplant;
    }

    public String getSystemState() {
        return systemState;
    }

    public void setSystemState(String systemState) {
        this.systemState = systemState;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getInterventionText() {
        return interventionText;
    }

    public void setInterventionText(String interventionText) {
        this.interventionText = interventionText;
    }

    public String getFaultText() {
        return faultText;
    }

    public void setFaultText(String faultText) {
        this.faultText = faultText;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRotabileSeries() {
        return rotabileSeries;
    }

    public void setRotabileSeries(String rotabileSeries) {
        this.rotabileSeries = rotabileSeries;
    }

    public String getLimitation() {
        return limitation;
    }

    public void setLimitation(String limitation) {
        this.limitation = limitation;
    }

    public ArrayList<Intervento> getInteventionList() {
        return inteventionList;
    }

    public void setInteventionList(ArrayList<Intervento> inteventionList) {
        this.inteventionList = inteventionList;
    }

    public ArrayList<TreeComponent> getTreeComponents() {
        return treeComponents;
    }

    public void setTreeComponents(ArrayList<TreeComponent> treeComponents) {
        this.treeComponents = treeComponents;
    }

    public ArrayList<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<Attachment> attachments) {
        this.attachments = attachments;
    }

    public String getAvvisiChildText() {
        return avvisiChildText;
    }

    public void setAvvisiChildText(String avvisiChildText) {
        this.avvisiChildText = avvisiChildText;
    }

    private void setAvvisiChildText() {
        setAvvisiChildText(this.getType() + " - " + this.getShortText());
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public Boolean getBlockedBool() {
        if (blocked == 0) return false;
        return true;
    }

    public void setBlockedBool(Boolean blockedBool) {
        if (blockedBool) blocked = 1;
        blocked = 0;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        if (status == null) {
            this.status = this.workOrderNumber;
        }
        this.status = status;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        setCreationDate();
    }
}
