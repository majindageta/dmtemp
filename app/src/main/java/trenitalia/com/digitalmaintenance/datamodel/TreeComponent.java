package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by davidericci on 25/08/17.
 */

public class TreeComponent implements Parcelable {

    private String code;
    private String description;

    public TreeComponent() {
    }

    public static final Creator<TreeComponent> CREATOR = new Creator<TreeComponent>() {
        @Override
        public TreeComponent createFromParcel(Parcel in) {
            return new TreeComponent(in);
        }

        @Override
        public TreeComponent[] newArray(int size) {
            return new TreeComponent[size];
        }
    };

    protected TreeComponent(Parcel in) {
        code= in.readString();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(description);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
