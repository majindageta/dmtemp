package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by davidericci on 24/08/17.
 */

public class Role implements Parcelable {
    private String roleName;
    private String roleCode;
    private Boolean enabled;

    public Role() {}

    public static final Creator<Role> CREATOR = new Creator<Role>() {
        @Override
        public Role createFromParcel(Parcel in) {
            return new Role(in);
        }

        @Override
        public Role[] newArray(int size) {
            return new Role[size];
        }
    };

    protected Role(Parcel in) {
        roleName = in.readString();
        roleCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(roleName);
        dest.writeString(roleCode);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
