package trenitalia.com.digitalmaintenance.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by davidericci on 24/08/17.
 */

public class Implants implements Parcelable {

    private String implantsName;
    private String implantsCode;
    private String implantsLocation;
    private ArrayList<Role> roles;

    private String sectionTitle;
    private String rotabileTitle;
    public Implants() {

    }

    public static final Creator<Implants> CREATOR = new Creator<Implants>() {
        @Override
        public Implants createFromParcel(Parcel in) {
            return new Implants(in);
        }

        @Override
        public Implants[] newArray(int size) {
            return new Implants[size];
        }
    };

    protected Implants(Parcel in) {
        implantsName = in.readString();
        implantsCode = in.readString();
        implantsLocation = in.readString();
        sectionTitle = in.readString();
        rotabileTitle = in.readString();
        roles = in.readArrayList(Role.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(implantsName);
        dest.writeString(implantsCode);
        dest.writeString(implantsLocation);
        dest.writeString(sectionTitle);
        dest.writeString(rotabileTitle);
        dest.writeList(roles);
    }

    public String getImplantsName() {
        return implantsName;
    }

    public void setImplantsName(String implantsName) {
        this.implantsName = implantsName;
        setActivityTitles();
    }

    public String getImplantsCode() {
        return implantsCode;
    }

    public void setImplantsCode(String implantsCode) {
        this.implantsCode = implantsCode;
        setActivityTitles();
    }

    public String getImplantsLocation() {
        return implantsLocation;
    }

    public void setImplantsLocation(String implantsLocation) {
        this.implantsLocation = implantsLocation;
        setActivityTitles();
    }

    public ArrayList<Role> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<Role> roles) {
        this.roles = roles;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    public String getRotabileTitle() {
        return rotabileTitle;
    }

    public void setRotabileTitle(String rotabileTitle) {
        this.rotabileTitle = rotabileTitle;
    }

    public void setActivityTitles() {
        setSectionTitle(this.implantsName + " - " + this.implantsLocation + " - " + this.implantsCode);
        setRotabileTitle("Rotabili in " + this.implantsName + " - " + this.implantsCode);
    }
}
